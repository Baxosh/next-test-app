export interface ICar {
    id: number
    name: string
    price: number
    image: string
}

export interface ICarList {
    cars: ICar[]
}

export interface ICarSingle {
    car: ICar
}