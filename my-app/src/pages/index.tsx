import {GetServerSidePropsResult, NextPage} from 'next'
import React from "react";
import Home from "@/components/screens/home/Home";
import {ICarList} from "@/types/car";
import CarService from "@/services/car.service";

const HomePage: NextPage<ICarList> = ({cars}) => {
    return (
        <Home cars={cars}/>
    )
}

// HomePage.getInitialProps = async (contenxt: NextPageContext) => {
//     const cars = await CarService.getAll()
//     console.log(cars[0])
//     return {
//         props: { cars }
//     }
// }

// export const getStaticProps: GetStaticProps<ICarList> = async () => {
//     const cars = await CarService.getAll()
//
//     console.log(cars[0])
//     return {
//         props: {cars},
//         revalidate: 60
//     }
// }

export const getServerSideProps = async () => {
    const cars = await CarService.getAll()
    return {
        props: { cars }
    }
}

export default HomePage