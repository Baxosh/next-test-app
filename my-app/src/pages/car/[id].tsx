import {GetStaticPaths, GetStaticProps, NextPage} from 'next'
import {ICarSingle} from "@/types/car";
import CarService from "@/services/car.service";
import CarItem from "@/components/ui/car/CarItem";
import {ParsedUrlQuery} from "querystring";

const CarDetailPage: NextPage<ICarSingle> = ({car}) => {
    return (
        <CarItem car={car}/>
    )
}

export default CarDetailPage

export interface Params extends ParsedUrlQuery {
    id: string
}

export const getStaticPaths: GetStaticPaths<Params> = async () => {
    const {cars} = await CarService.getAll()

    return {
        paths: cars.map(car => ({
            params: {
                id: car.id.toString()
            }
        })),
        fallback: 'blocking',
    }
}

export const getStaticProps: GetStaticProps<ICarSingle> = async ({params}) => {
    const car = await CarService.getById(String(params?.id))

    return {
        props: {car},
        revalidate: 60
    }
}

