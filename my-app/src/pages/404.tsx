import {NextPage} from "next";
import Image from "next/image";
import Layout from "@/components/layout/Layout";

const NotFound: NextPage = () => {
    return (
        <Layout className="flex flex-col items-center justify-center w-screen h-[70vh]">
            <Image priority src="/404.png" alt="404 page" width="400" height="200" />
            <h1 className="text-8xl font-bold text-center text-[#da5129e6] underline">Страница не найдена</h1>
        </Layout>
    )
}

export default NotFound