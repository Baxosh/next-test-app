import axios from "axios";
import {ICarList, ICarSingle} from "@/types/car";

axios.defaults.baseURL = process.env.API_URL

const CarService = {
    async getAll() {
        const {data} = await axios.get<ICarList>("/cars")
        return data
    },
    async getById(id: string) {
        const {data} = await axios.get<ICarSingle>("/cars", {
            params: {id}
        })
        return data[0]
    }
}

export default CarService