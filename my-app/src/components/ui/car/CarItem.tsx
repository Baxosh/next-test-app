import React, {FC} from 'react';
import {ICarSingle} from "@/types/car";
import Image from "next/image"
import Link from "next/link";

export interface Props extends ICarSingle {}

const CarItem: FC<Props> = ({car}) => {
    return (
        <div>
            <Image priority  className="w-auto h-auto" src={car.image}  alt={car.name} width="300" height="200" />
            <h2 className="title">{car.name}</h2>
            <small>{car.price}</small>

            <Link href={`/car/${car.id}`}>Read more</Link>
        </div>
    );
};

export default CarItem;