import React, {FC} from "react";
import Layout from "@/components/layout/Layout";
import {ICarList} from "@/types/car";
import CarItem from "@/components/ui/car/CarItem";

// const inter = Inter({subsets: ['latin']}) TODO: use own font

export default function Home({ cars }: ICarList) {
    console.log('hi')
    return (
        <Layout title="Home" description="lorem ipsum 20">
            <h1>Hello, World!</h1>

            {cars.length ? cars.map(car => <CarItem key={car.id} car={car} />) : <div>Cars not found</div>}
        </Layout>
    )
}
