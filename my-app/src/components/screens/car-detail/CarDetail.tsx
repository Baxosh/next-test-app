import {NextPage} from 'next'
import {ICarSingle} from "@/types/car";
import Layout from "@/components/layout/Layout";
import CarItem from "@/components/ui/car/CarItem";

const CarDetail: NextPage<ICarSingle> = ({car}) => {
    return (
        <Layout title={car.name}>
            <CarItem car={car}/>
        </Layout>
    )
}

export default CarDetail