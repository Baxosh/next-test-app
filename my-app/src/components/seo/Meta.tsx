import React, {FC, Fragment, PropsWithChildren} from "react";
import Head from "next/head";

const Meta: FC<PropsWithChildren<IMeta>> = ({title, description, children}) => {
    return (
        <Fragment>
            <Head>
                <title>{title}</title>
                {description ? (
                    <>
                        <meta name="description" content={description} />
                        <meta name="og:title" content={title} />
                        <meta name="og:description" content={description} />
                    </>
                ) : (
                    <meta name="robots" content="noindex, nofollow" />
                )
                }
            </Head>
            {children}
        </Fragment>
    )
}

export default Meta