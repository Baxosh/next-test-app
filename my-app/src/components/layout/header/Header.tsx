import React from 'react';
import Link from "next/link";
import {useRouter} from "next/router";
import {clsx as cn} from "clsx"

export default function Header({}): JSX.Element {
    const {pathname} = useRouter()
    return (
        <header className='header'>
            <Link
                className={cn('button', 'gray', pathname === '/' && 'active')}
                href="/">
                Home
            </Link>

            <Link
                className={cn('button', 'gray', pathname === '/about' && 'active')}
                href="/about">
                About
            </Link>
        </header>
    );
}
