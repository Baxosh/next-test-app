import React, {FC, Fragment, PropsWithChildren, ReactNode} from "react";
import Header from "@/components/layout/header/Header";
import Meta from "@/components/seo/Meta";

export type Props = IMeta & {
    children?: ReactNode,
    className?: string
}

export default function Layout({children, className, title, description}: Props) {
    return (
        <Meta title={title} description={description}>
            <Header/>

            {className ? (
                <div className={className}>
                    {children}
                </div>
            ) : children}
        </Meta>
    )
}
